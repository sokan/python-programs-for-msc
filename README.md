In this repository I will upload the homework essays done during the MSc course "Programming languages and software tools for Bioinformatics II".

The whole work was done by creating a virtual end by `python -m venv <DIR_NAME>`, <DIR_NAME>=venv
In requirements.txt are the packages needed for the programs to work. To install them run `pip install -r requirements.txt`.

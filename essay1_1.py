#########################################
#   python program that prints the sum  #
#       of the elements of a list       #
#########################################

num_list = [1,2,3,4,5,6,8]
total = sum(num_list) 

print("The sum of the elements of the list is: ", total) 

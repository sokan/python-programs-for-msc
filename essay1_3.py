# Remove duplicates from a list and print "clean" list #

num_list = [11,2,3,4,11,43,23,13,4,1,2,3,4,1,90,90,99,53,6,123,0]
clean_num_list = list()

for i in num_list:
    if i not in clean_num_list:
        clean_num_list.append(i)

print("Cleaned list is: ", clean_num_list)

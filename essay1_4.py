# count frequence of a character occurence in a string and print them #

string = "KJHALSKDJFHLKSADHFLKJSAHFLIWUEHFLIAUWHFELIUWHFELAIUHFLAIUWHFLIWAHFELIWAUHFELIAUHFELIHMXZNBCLKSHFDMNBXCZMVLAKSFDDSZXMBVC"

frequencies = {}

for i in string:
    if i in frequencies:
        frequencies[i] += 1
    else:
        frequencies[i] = 1

print ("The frequences of each character in the string are: \n", frequencies)

# Check if a string is a palindrome and print True/False (boolean) #

def palindrome(string):
    for i in range(0, len(string)):
            if string[i] != string[-i-1]:
                return False
            else:
                return True

print("Enter sequence to check if it's a palindrome: ")
string = input()

if (palindrome(string)):
    print("It is.")
else:
    print("It is not.")

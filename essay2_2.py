# program to check if a number is prime or not #

print("Enter an integer to check if it's a prime number: ")
num = int(input())

def isprime(number):
    if number > 1:
        for i in range(2, int(number/2)+1):
            if (number % i) == 0:
                print(number, "is not a prime number")
                break
            else:
                print(number, "is a prime number")
                break
    else:
        print(number, "is not a prime numer")

def print_primes():
    primes_list = []
    for i in range(2, 1000):
        for j in range(2, int(i/2)+1):
            if i % j == 0:
                break
        else:
            primes_list.append(i)
    print ("Prime numbers in [2,1000]: ", primes_list)

isprime(num) 
print_primes()

# Write a class circle that 

import math

class circle():
    def __init__(self, radius, centre_x, centre_y):
        self.radius = radius
        self.centre_x = centre_x
        self.centre_y = centre_y
    
    def surface(self):
        print("Surface of the circle is: ", math.pi*(self.radius**2))
    
    def perimetre(self):
        print("Perimetre of the circle is: ", 2*math.pi*self.radius)
    
    def is_inside(self, x, y):
        self.x = x
        self.y = y
        if (((x - self.centre_x)**2 + (y - self.centre_y)**2) <= self.radius**2):
            print("Point is inside the circle.")
        else:
            print("Point is outside of the circle.")

centre_x = 0
centre_y = 1
x = 10
y = 1
radius = 3

my_circ = circle(radius, centre_x, centre_y)
my_circ.surface()
my_circ.perimetre()
my_circ.is_inside(x,y)

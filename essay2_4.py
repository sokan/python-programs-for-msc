# python program that constructs a 
# 2D array as a chessboard with 
# white = 0 and black = 1

import numpy as np
n = 10 

# Create a 2D array
chess = np.zeros((n,n))

chess[::2, ::2] = 1
chess[1::2, 1::2] = 1

print(chess)

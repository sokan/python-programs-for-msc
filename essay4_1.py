# Write a Python program that asks the user for two DNA sequences, 
# and prints the reverse complement of their concatenation.

from Bio.Seq import Seq

print("Enter first sequence of DNA in capital letters: ")
input1 = input()
seq1 = Seq(input1)

print("Enter second DNA sequence: ")
input2 = input()
seq2 = Seq(input2)

seqconc = seq1 + seq2

print("Reverse complementary sequence is: ",seqconc.reverse_complement())

# Write a Python program that asks the user for two DNA sequences, 
# and prints the reverse complement of their concatenation.

from Bio.Seq import Seq

while True:
    print("Enter first sequence of DNA in capital letters: ")
    input1 = input()
    if all ([char in ["A","C","G","T"] for char in input1]):
        break

seq1 = Seq(input1)

while True:
    print("Enter second DNA sequence: ")
    input2 = input()
    if all ([char in ["A","C","G","T"] for char in input2]):
        break

seq2 = Seq(input2)

seqconc = seq1 + seq2

print("Reverse complementary sequence is: ",seqconc.reverse_complement())

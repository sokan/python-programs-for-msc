#Write a Python program that asks the user for a DNA sequence, 
#and prints both the corresponding mRNA sequence and protein sequence, 
#including stop codons (according to the standard translation table).

from Bio.Seq import Seq

print("Enter DNA sequence: ")
my_input = input()
my_seq = Seq(my_input)

#mRNA sequence
mrna = my_seq.transcribe()
print("mRNA is: ", mrna)

#protein sequence
protein = my_seq.translate(stop_symbol="~")
print("Protein translated it: ", protein)

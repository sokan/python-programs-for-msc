#Write a Python program that asks the user for a DNA sequence, 
#and prints both the corresponding mRNA sequence and protein sequence, 
#including stop codons (according to the Yeast Mitochondrial Code translation table).

from Bio.Seq import Seq
from Bio.Data import CodonTable

print("Enter DNA sequence: ")
my_input = input()
my_seq = Seq(my_input)

#mRNA sequence
mrna = my_seq.transcribe()
print("mRNA is: ", mrna)

#protein sequence
protein = my_seq.translate(table=CodonTables.unambiguous_dna_by_name["Yeast Mitochondrial", stop_symbol="~")
print("Protein translated it: ", protein)

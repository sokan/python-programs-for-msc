#Write a Python program that takes the sequence of the 1AI4 PDB protein 
#(download the FASTA file manually), and writes a corresponding UniProt file.

from Bio import SeqIO
from Bio.Seq import Seq

in_handle = open("rcsb_pdb_1AI4.fasta", "r")
out_handle = open("rcsb_pdb_1AI4.embl", "wb")

records = list(SeqIO.parse(in_handle, "fasta"))

for seq_record in records:
    seq_record.annotations["molecule_type"] = "protein"

count = SeqIO.write(records, out_handle, "seqxml")

print(count)

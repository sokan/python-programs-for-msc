#Write a Python program that takes the sequences.fasta file and 
#writes N single-sequence FASTA files, called sequence{number}.fasta, 
#each one containing a single sequence of the original file.

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

in_handle = open("sequences.fasta", "r")

counter = 0

for seq_record in SeqIO.parse(in_handle, "fasta"):
    counter = counter + 1
    out_handle = open("sequence{%d}.fasta" % counter , "w")
    SeqIO.write(seq_record, out_handle, "fasta")

    out_handle.close()

print("Created %d .fasta files." % counter)

in_handle.close()

#Write a Python program that takes the dnasequences.fasta file 
#and writes a revcomp.fasta file with the reverse complements 
#of the original sequences.

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

in_handle = open("dnasequences.fasta", "r")
out_handle = open("revcomp.fasta", "w")

for seq_record in SeqIO.parse(in_handle, "fasta"):
    seq_record = seq_record.reverse_complement(id="rc_"+seq_record.id, description = "reverse complement")
    SeqIO.write(seq_record, out_handle, "fasta")

out_handle.close()
in_handle.close()

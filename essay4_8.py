#Find and download a single sequence record from genbank. 
#The genbank identifier of the record is HE805982.1. 
#This record contains information about the DNA region coding for HBx, 
#a multifunctional hepatitis B viral protein involved in modulating 
#several pathways by directly or indirectly interacting with hosts factors 
#(protein degradation, apoptosis, transcription, signal transduction, 
#cell cycle progress, and genetic stability). 
#Write a Python program that, using the genbank record, saves the corresponding 
#protein sequence in fasta format.

from Bio import SeqIO
from Bio import Entrez
from Bio.SeqRecord import SeqRecord

gbk_filename = "HE805982.1"
faa_filename = "%s.fasta" % gbk_filename

Entrez.email = 'dnakos@biol.uoa.gr'

in_handle = Entrez.efetch(db="nucleotide", rettype="fasta", id=gbk_filename)
out_handle = open(faa_filename, "w")

seq_record = SeqIO.read(in_handle,"fasta")

seq_record.seq = seq_record.seq.translate(table='Standard', stop_symbol='~')

SeqIO.write(seq_record, out_handle, "fasta")

out_handle.close()
in_handle.close()

#Write a program that, given a fasta file containing multiple protein sequences 
#and a string specified by the user, prints to a new file only sequences that 
#contain at least one occurrence of the string. Test your program 
#with the file sequences.fasta, printing sequences containing a stretch of at 
#least three glutamines.

from Bio import SeqIO
from Bio import Seq

in_handle = open("sequences.fasta", "r")
out_handle = open("proteinmotifs.fasta", "w")

motif = input("Enter string to scan for: ")

counter = 0

for seq_record in SeqIO.parse(in_handle, "fasta"):
    if motif in seq_record.seq:
        counter = counter + 1
        if counter >= 1:
            SeqIO.write(seq_record, out_handle, "fasta")

print("%d sequences were found in sequences.fasta. You can find them in proteinmotifs.fasta" % counter)

out_handle.close()
in_handle.close()
